require 'Manager/RuleProcessItem'
require 'Entity/Rule'

class MarketController < ApplicationController
  def Index
  		
	listRulesItems = Array.new
	
	listRulesItems.push(Rule.new('A',50, 3, 20))
	listRulesItems.push(Rule.new('B',30, 2, 15))
	listRulesItems.push(Rule.new('C',20, 0, 0))
	listRulesItems.push(Rule.new('D',15, 0, 0))	

	co = RuleProcessItem.new(listRulesItems)
	
	co.scan('A')
	co.scan('A')
	co.scan('A')
	co.scan('A')
	co.scan('D')
	co.scan('D')
	co.scan('A')
	co.scan('B')
	co.scan('B')
	co.scan('B')
	co.scan('C')
	co.scan('C')
	co.scan('A')
	co.scan('A')	
	co.scan('B')
	co.scan('B')
	co.scan('B')
	co.scan('B')
		
	@total = co.instance_variable_get(:@totalPrice)
	
  end
end