class RuleProcessItem		
	@@listRulesItems = Array.new
	@@listselectedsItems = Array.new
			
	def initialize(listRulesItems)
		@@listRulesItems = listRulesItems
		@@listselectedsItems = Array.new
		@totalPrice = 0
	end
	
	def scan(nameItem)
		countItems  = 0
		total  = @totalPrice
				
		item = @@listRulesItems.find {|s| s.nameItem == nameItem }	
		
		countItems = countSelectedsItems(nameItem)
		
		total = total + item.priceItem
		
		if item != nil and (item.countItemsToDiscount > 0 and (countItems % item.countItemsToDiscount == 0))
			total = total - item.valueToDiscount		
		end

		@totalPrice = total			
	end
	
	def countSelectedsItems(nameItem)
		@@listselectedsItems.push(nameItem)
		
		return @@listselectedsItems.count{|item| item == nameItem}	
	end
end
