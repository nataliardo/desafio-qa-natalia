class Rule		
	attr_accessor:nameItem
	attr_accessor:priceItem
	attr_accessor:countItemsToDiscount
	attr_accessor:valueToDiscount

	def initialize(nameItem, priceItem, countItemsToDiscount, valueToDiscount)
		@nameItem = nameItem
		@priceItem = priceItem
		@countItemsToDiscount = countItemsToDiscount
		@valueToDiscount = valueToDiscount
	end
end