require 'test_helper'
require 'Entity/Rule'


class MarketControllerTest < ActionController::TestCase
    
	@@listRulesItems = Array.new
	
	@@listRulesItems.push(Rule.new('A',50, 3, 20))
	@@listRulesItems.push(Rule.new('B',30, 2, 15))
	@@listRulesItems.push(Rule.new('C',20, 0, 0))
	@@listRulesItems.push(Rule.new('D',15, 0, 0))	
	
	
	def price(goods)
        co = RuleProcessItem.new(@@listRulesItems)
        goods.split(//).each { |item| co.scan(item) }		
        co.instance_variable_get(:@totalPrice) 
    end

    def test_totals
        assert_equal(  0, price(""))
        assert_equal( 50, price("A"))
        assert_equal( 80, price("AB"))
        assert_equal(115, price("CDBA"))

        assert_equal(100, price("AA"))
        assert_equal(130, price("AAA"))
        assert_equal(180, price("AAAA"))
        assert_equal(230, price("AAAAA"))
        assert_equal(260, price("AAAAAA"))

        assert_equal(160, price("AAAB"))
        assert_equal(175, price("AAABB"))
        assert_equal(190, price("AAABBD"))
        assert_equal(190, price("DABABA"))
    end

    def test_incremental
        co = RuleProcessItem.new(@@listRulesItems)
        assert_equal( 0, co.instance_variable_get(:@totalPrice))
        co.scan("A"); assert_equal( 50, co.instance_variable_get(:@totalPrice))
        co.scan("B"); assert_equal( 80, co.instance_variable_get(:@totalPrice))
        co.scan("A"); assert_equal(130, co.instance_variable_get(:@totalPrice))
        co.scan("A"); assert_equal(160, co.instance_variable_get(:@totalPrice))
        co.scan("B"); assert_equal(175, co.instance_variable_get(:@totalPrice))
    end 
end